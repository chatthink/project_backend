const id = '123';
const collection = 'users';
module.exports = {
  async up(db, logger) {
    logger.info('Creating user %s', id);

    await db.collection(collection).insertOne({
      id, name: 'migration1', email: 'migration1@gmail.com', password: 'ceciestunmotdepassedifficile',
    });
  },

  async down(db, logger) {
    logger.warn('Removing user %s', id);

    await db.collection(collection).removeOne({ id });
  },
};
