const id = '123';
const collection = 'users';
module.exports = {
  async up(db) {
    await db.collection(collection).insertOne({
      id, name: 'migration1', email: 'migration1@gmail.com', password: 'ceciestunmotdepassedifficile',
    });
  },

  async down(db) {
    await db.collection(collection).removeOne({ id });
  },
};
