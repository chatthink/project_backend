# ChatThink Backend
This is a Node.js TypeScript Express API with two environments: development and production. It is designed to be run using Docker.

The API is deployed at https://api.mintsoulz.click.

## Requirements

- Docker
- Docker Compose

## Usage

To start the production environment, run:

```bash
./start_prod.sh
```

To start the development environment, run:

```bash
./start_dev.sh
```

# Production Server

The production server is an Ubuntu 20.04 server with Docker and NGINX installed. NGINX is used as a reverse proxy to serve the API, and SSL certificates were obtained using Certbot.

# CI/CD

The API is deployed using GitLab CI/CD. An SSH key was added to allow the pipeline to connect to the deployment server.

The pipeline begins with lint testing to ensure code cleanliness. Then, the pipeline connects to the deployment server, pulls the latest version of the repository, and restarts the API.

# Migrations

1. Install mongodb-migrations using npm: `npm install -g mongodb-migrations`

2. Create a configuration file for your project using the following command: `mongodb-migrations init`

3. This configuration file will contain the login information to your MongoDB database, as well as other operational settings. Modify it according to your needs.

4. Create a migration folder in your project using the following command: `mongodb-migrations create <migration name>`

5. This folder will contain your migration script. You can use the node.js mongodb library to execute data editing commands on your database.

6. When you are ready to perform your migration, use the following command: `mongodb-migrations up`

This will execute all migrations that have not yet been applied to your database. You can also use the mongodb-migrations down command to cancel a migration.

