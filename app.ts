const express = require('express');

// Require the CORS module to allow cross-origin resource sharing
const cors = require('cors');

// Require the cookie-parser module to parse cookies
const cookieParser = require('cookie-parser');

// Require the path module to work with file and directory paths
const path = require('path');

// Require the database configuration file
require('./src/db/config');

// Require the dotenv module and its config method to load environment variables from a .env file
require('dotenv').config();

// Create an instance of the Express app
const app = express();

// Create an HTTP server using the Express app
const server = require('http').createServer(app);

// Create a Socket.IO server using the HTTP server
const io = require('socket.io')(server, {
  // Allow connections from the specified origin URL
  cors: {
    origin: process.env.CLIENT_URL,
    // Allow the passing of credentials (cookies, HTTP authentication)
    credentials: true,
  },
});

// Require the user routes file
const userRoutes = require('./src/routes/user');

// Enable CORS for the client specified in the CLIENT_URL environment variable
app.use(cors({ credentials: true, origin: process.env.CLIENT_URL }));

// Parse cookies from the request headers
app.use(cookieParser());

// Parse request bodies as JSON
app.use(express.json());

// Use the user routes for requests to the '/api' path
app.use('/api', userRoutes);

// Declares a variable users and assigns it an empty array.
let users = [];

// Add a new user to the list of users
const addUser = (userId: any, username: any, socketId: any) => {
  // Check if the userId is unique and push the new user object to the array
  // If the user was added, the result of push will be the
  // new length of the array, which will evaluate to true
  // If the user was not added, the result of push will be undefined, which will evaluate to false
  const listUser = !users.some((user) => user.userId === userId)
    && users.push({ userId, username, socketId });

  // Return the result of the push operation
  return listUser;
};

// Find a user in the list of users by userId
const getUser = (userId: any) => users.find((user) => user.userId === userId);

// Remove a user from the list of users by socketId
const removeUser = (socketId: any): any => {
  // Use the filter function to create a new array containing
  // only the users whose socketId does not match the one to be removed
  users = users.filter((user) => user.socketId !== socketId);
};

// Start Socket
io.on('connection', (socket: any) => {
  // This event listener is triggered when a new client connects to the server
  // The callback function is passed a socket object representing the connection to the client
  // This event listener is triggered when the client emits an 'adduser' event
  socket.on('adduser', (userId: any, username: any) => {
    // Add the user to the list of users
    addUser(userId, username, socket.id);
    // Emit an 'getusers' event to all clients with the updated list of users
    io.emit('getusers', users);
  });
  // This event listener is triggered when the client emits a 'privatemsg' event
  socket.on('privatemsg', (message: any) => {
    try {
      // Find the socket object for the recipient of the message
      const onlineFriend = getUser(message.receiver);
      // Send the message to the recipient using their socket object
      io.to(onlineFriend.socketId).emit('privatemessage', message);
    } catch (err) {
      // stderr.write(err);
    }
  });

  // This event listener is triggered when the client disconnects from the server
  socket.on('disconnect', () => {
    // Remove the client from the list of users
    removeUser(socket.id);
    // Emit an 'getusers' event to all clients with the updated list of users
    io.emit('getusers', users);
  });
  // This event listener is triggered when the client emits a 'disconnectUser' event
  socket.on('disconnectUser', (userId: any) => {
    try {
      // Remove the specified user from the list of users
      const filterUsers = users.filter((user) => user.userId !== userId);
      users = filterUsers;
      // Emit an 'getusers' event to all clients except
      // the one that emitted the event, with the updated list of users
      socket.broadcast.emit('getusers', users);
    } catch (err) {
      // stderr.write(err);
    }
  });
});

if (process.env.NODE_ENV === 'production') {
  // In production, serve the static files from the 'client/build' directory
  app.use(express.static('client/build'));
  // All remaining requests return the React app, so it can handle the routing
  app.get('*', (req: any, res: any) => {
    res.sendFile(path.resolve(__dirname, './client/build', 'index.html'));
  });
}
// The port the server will listen on
const port = (process.env.PORT as string) || 3001;
// Start the server
server.listen(port, () => {
});
