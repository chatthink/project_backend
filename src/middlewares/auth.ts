// Import the `jwt` module
const jwt = require('jsonwebtoken');

// Load the environment variables from the .env file
require('dotenv').config();

// This is a middleware function that
// checks the user's authentication status based on a JSON web token (JWT)
const auth = (req: any, res: any, next: any) => {
  // Try-catch block to handle any errors that may occur
  try {
    // Get the JWT from the `jwt` cookie
    const token = req.cookies.jwt;

    // If there is no JWT, return a 401 status code and an error message
    if (!token) {
      return res
        .status(401)
        .json({ msg: 'No authentication token, authorization denied.' });
    }

    // Verify the JWT using the `jwt.verify` method and the `JWT_TOKEN`
    // environment variable as the secret
    const verified = jwt.verify(token, process.env.JWT_TOKEN);

    // If the JWT cannot be verified, return a 401 status code and an error message
    if (!verified) {
      return res
        .status(401)
        .json({ msg: 'Token verification failed, authorization denied.' });
    }

    // If the JWT is verified, add the `id` property of the payload to the `user`
    // property of the `req` object, and add the `token` to the `token` property
    // of the `req` object
    req.user = verified.id;
    req.token = token;

    // Call the next middleware function
    next();
  } catch (err) {
    // If there is an error, return a 500 status code and the error message
    res.status(500).json({ error: err.message });
  }
  // Return the `res` object
  return res;
};

// Export the `auth` function
module.exports = auth;
