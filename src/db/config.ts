// Import the `mongoose` module
const mongoose = require('mongoose');

// Load the environment variables from the .env file
require('dotenv').config();

// Connect to the MongoDB database
mongoose.connect(
  // Use the `MONGODB_URL` environment variable as the URL for the MongoDB connection
  process.env.MONGODB_URL as string,
  // Pass in options for the connection
  {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  },
  // An empty function is passed as the callback for the `connect` method
  () => {},
);
