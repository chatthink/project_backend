// Import the `mongoose` module
const mongooseSchema = require('mongoose');

// Define a schema for users
const userSchema = mongooseSchema.Schema({
  username: {
    type: String,
    required: true,
  },
  email: {
    type: String,
    required: true,
  },
  password: {
    type: String,
    required: true,
    min: 6,
  },
  date: {
    type: String,
    default: new Date().toDateString(),
  },
});

// Export the model with the name 'Users' and the defined schema
module.exports = mongooseSchema.model('Users', userSchema);
