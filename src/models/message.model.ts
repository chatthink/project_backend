// Import the `mongoose` module
const mongooseMessage = require('mongoose');

// Define a schema for private messages
const privateMessageSchema = mongooseMessage.Schema(
  {

    author: { type: String, required: true },
    authorId: { type: String, required: true },
    content: { type: String },
    receiver: { type: String, required: true },
    receiverName: { type: String, required: true },
    participants: [String],
    type: { type: String },
  },
  // Add timestamps to the document
  { timestamps: true },
);

// Export the model with the name `PrivateMessages` and the defined schema
module.exports = mongooseMessage.model('PrivateMessages', privateMessageSchema);
