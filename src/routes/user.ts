// Import the `express` router
const router = require('express').Router();

// Import the `bcrypt` module for password hashing
const bcrypt = require('bcrypt');

// Import the `multer` module for handling file uploads
const multer = require('multer');

// Import the `CloudinaryStorage` module from the `multer-storage-cloudinary` package
const { CloudinaryStorage } = require('multer-storage-cloudinary');

// Import the `cloudinary` module
const cloudinary = require('cloudinary').v2;

// Import the `jwt` module for generating JSON web tokens
const jwtToken = require('jsonwebtoken');

// Import the `UserModel` module for interacting with the user data in the database
const UserModel = require('../models/user.model');

// Import the `PrivateMessages` model for interacting with the private messages data in the database
const Privatemessage = require('../models/message.model');

// Import the `auth` middleware function for checking the user's authentication status
const userAuth = require('../middlewares/auth');

// Load the environment variables from the .env file
require('dotenv').config();

// Configure the `cloudinary` module using the environment variables
cloudinary.config({
  cloud_name: process.env.CLOUDINARY_NAME,
  api_key: process.env.CLOUDINARY_API_KEY,
  api_secret: process.env.CLOUDINARY_API_KEY_SECRET,
});

// Create a new `CloudinaryStorage` instance for storing uploaded files on Cloudinary
const storage = new CloudinaryStorage({
  cloudinary,
  // Set the folder where the uploaded files should be stored
  params: {
    folder: 'attachments',
    // Set the resource type to `auto` so that
    // Cloudinary can determine the type of the file being uploaded
    resource_type: 'auto',
    // Set the public ID of the file to the filename
    public_id: (req: any, file: any) => file.filename,
  },
});

// Create a new `multer` instance for handling file uploads
const upload = multer({ storage });

// Set up a GET route for the `/auth` path
router.get('/auth', userAuth, async (req: any, res: any) => {
  // Try-catch block to handle any errors that may occur
  try {
    // Find the user with the `_id` property equal to the `id` property of the `req.user` object
    const user = await UserModel.findOne({ _id: req.user });
    // Send a 200 status code and the user object in the response
    res.status(200).json({
      user,
    });
  } catch (err) {
    // If there is an error, send a 400 status code and the error object in the response
    res.status(400).json(err);
  }
});

// Set up a POST route for the `/register` path
router.post('/register', async (req: any, res: any) => {
  // Async function to handle the request and response
  const emailExist = await UserModel.findOne({ email: req.body.email });

  // If a user with the same email exists, send a 400 status code and an error message
  if (emailExist) {
    return res.status(400).json({ message: 'Usersname/Email Already Exist' });
  }
  // Generate a salt for password hashing
  const salt = await bcrypt.genSalt(10);

  // Hash the password using the generated salt
  const hashPassword = await bcrypt.hash(req.body.password, salt);

  // Create a new user object with the received data
  const user = new UserModel({
    username: req.body.username,
    email: req.body.email,
    password: hashPassword,
  });

  // Try-catch block to handle any errors that may occur
  try {
    // Save the user object to the database
    const savedUser = await user.save();
    // Send the saved user object in the response
    res.json(savedUser);
  } catch (err) {
    // Send the saved user object in the response
    res.status(400).json(err);
  }
  // Send a 201 status code to indicate that the user was successfully created
  return res.status(201);
});

// Set up a POST route for the `/login` path
router.post('/login', async (req: any, res: any) => {
  // Find the user with the provided email
  const user = await UserModel.findOne({ email: req.body.email });

  // If no user with the provided email exists, send a 401 status code and an error message
  if (!user) {
    return res
      .status(401)
      .json({ message: 'Email or Password is incorrect !' });
  }

  // Compare the provided password to the hashed password in the database
  const validPass = await bcrypt.compare(req.body.password, user.password);

  // If the passwords don't match, send a 401 status code and an error message
  if (!validPass) {
    return res
      .status(401)
      .json({ message: 'Email or Password is incorrect !' });
  }

  // Generate a JSON web token using the user's `_id` property and the JWT secret
  const token = jwtToken.sign({ id: user._id }, process.env.JWT_TOKEN);

  // Set the token in a cookie and send it in the response
  res.cookie('jwt', token, {
    expires: new Date(Date.now() * 1000),
    httpOnly: true,
    secure: true,
  });
  res.json({ token, user, message: 'Login Successfully !' });

  return res;
});

// Set up a GET route for the `/logout` path
router.get('/logout', userAuth, async (req: any, res: any) => {
  // Try-catch block to handle any errors that may occur
  try {
    // Clear the `jwt` cookie
    res.clearCookie('jwt');
    // Send a 200 status code and a message indicating that the logout was successful
    res.status(200).send('Logout Successfully');
  } catch (error) {
    // If there is an error, send a 500 status code and the error object in the response
    res.status(500).send(error);
  }
});

// Set up a GET route for the `/users` path
router.get('/users', userAuth, async (req: any, res: any) => {
  // Try-catch block to handle any errors that may occur
  try {
    // Find all users in the database
    const users = await UserModel.find({});
    // Send a 200 status code and the array of users in the response
    res.status(200).json({
      users,
    });
  } catch (error) {
    // If there is an error, send a 500 status code and the error object in the response
    res.status(500).send(error);
  }
});

// Set up a GET route for the `/getallmessages` path
router.get('/getallmessages', userAuth, async (req: any, res: any) => {
  // Try-catch block to handle any errors that may occur
  try {
    // Find all private messages in the database
    const messages = await Privatemessage.find({});
    // Send a 200 status code and the array of messages in the response
    res.status(200).json({
      messages,
    });
  } catch (error) {
    // If there is an error, send a 500 status code and the error object in the response
    res.status(500).send(error);
  }
});

// Set up a GET route for the `/user/:id` path
router.get('/user/:id', async (req: any, res: any) => {
  // Destructuring the `id` parameter from the request object
  const { id } = req.params;
  // If the `id` parameter is present
  if (id) {
    // Find the user in the database by the provided `id`
    const foundUser = await UserModel.findById(id);
    // If the user was found
    if (foundUser) {
      // Send the user object in the response
      res.send(foundUser);
    }
  }
});

// Set up a GET route for the `/privatemessages` path
router.get('/privatemessages', async (req: any, res: any) => {
  // Find all private messages in the database that have both
  // the `friendid` and `userid` values in the `participants` array
  const foundMessages = await Privatemessage.find({
    $and: [
      { participants: { $in: [req.query.friendid] } },
      { participants: { $in: [req.query.userid] } },
    ],
  })
    // Sort the messages in descending order by the `createdAt` field
    .sort({ createdAt: -1 })
    // Limit the number of messages returned to 20
    .limit(20);
  // Reverse the order of the messages and send them in the response
  res.send(foundMessages.reverse());
});

// Declare a variable to store the new message object
let newMessage: any;

// Set up a POST route for the `/messages` path
router.post(
  '/messages',
  upload.single('content'),
  // Async function to handle the request and response
  async (req: any, res: any) => {
    try {
      // If a file was uploaded
      if (req.file) {
        // Create a new message object with the provided data,
        // including the file path, mime type, and participants array
        newMessage = new Privatemessage({
          author: req.body.author,
          authorId: req.body.authorId,
          content: req.file.path,
          receiver: req.body.receiver,
          receiverName: req.body.receiverName,
          participants: req.body.participants.split(','),
          type: req.file.mimetype,
        });
      } else { // If no file was uploaded
        // Create a new message object with the provided data
        newMessage = new Privatemessage(req.body);
      }
      // Try to save the new message to the database
      try {
        const savedMessage = await newMessage.save();
        // If the message was saved successfully, send a 200 status code
        // and the saved message as the response
        res.status(200).json(savedMessage);
      } catch (err) {
        // If an error occurred while saving the message,
        // send a 500 status code and the error as the response
        res.status(500).json(err);
      }
    } catch (err) {
      // stderr.write(err.response.body);
    }
  },
);

module.exports = router;
